# Notes

Please add here any notes, assumptions and design decisions that might help up understand your though process.


     Note: Looks like the Strategy and Chain of Responsibility patterns could be used here
     1. Modify the Item Interface to include a Category enum to classify the product and ultimately determine if the product qualifies for a discount
     2. Inject a Calculator interface with a PricingDiscountCalculator (impl) into the Basket class that takes List<Item> and
        returns a cumulative discount Price across all items or zero...may need a hash table in there to group by PricingDiscountScheme
     3. Add a PricingDiscountProcessor that implements N number of implementations which takes a PricingDiscountScheme enum and a List<Item>
     4. Implement a couple of these PricingDiscountProcessors
     5. Modify the TotalCalculator.discount to invoke the PricingDiscountCalculator and return the discount amount
     *
     Extra... Add PricingDiscountScheme enum list to the Basket to toggle which discounts are being offered for a given basket.

 Unfinished work
 
     Wanted to inject the Processors into the PricingDiscountItemCalculator using a factory if time permitted