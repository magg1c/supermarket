package kata.supermarket;

import kata.supermarket.discounting.PricingDiscountScheme;

import java.math.BigDecimal;

public class ItemByUnit implements Item {

    private final Product product;
    private final PricingDiscountScheme pricingDiscountScheme;

    ItemByUnit(final Product product) {
        this.product = product;
        this.pricingDiscountScheme = Item.super.getPricingDiscountScheme();
    }
    ItemByUnit(final Product product, final PricingDiscountScheme pricingDiscountScheme) {
        this.product = product;
        this.pricingDiscountScheme = pricingDiscountScheme;
    }

    public BigDecimal price() {
        return product.pricePerUnit();
    }

    public PricingDiscountScheme getPricingDiscountScheme() {
        return pricingDiscountScheme;
    }

}