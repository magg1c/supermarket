package kata.supermarket;

import java.math.BigDecimal;

public class ItemByWeight implements Item {

    private final WeighedProduct product;
    private final BigDecimal weightInKilos;
    private Category category;
    private ItemIdentifier itemIdentifier;

    ItemByWeight(final WeighedProduct product, final BigDecimal weightInKilos) {
        this.product = product;
        this.weightInKilos = weightInKilos;
        this.category = Item.super.getCategory();
    }

    ItemByWeight(final WeighedProduct product, final BigDecimal weightInKilos, final Category category) {
        this.product = product;
        this.weightInKilos = weightInKilos;
        this.category = category;
        this.itemIdentifier = ItemIdentifier.UNDEFINED;
    }

    ItemByWeight(final WeighedProduct product, final BigDecimal weightInKilos, final Category category, final ItemIdentifier itemIdentifier) {
        this.product = product;
        this.weightInKilos = weightInKilos;
        this.category = category;
        this.itemIdentifier = itemIdentifier;
    }

    public BigDecimal price() {
        return product.pricePerKilo().multiply(weightInKilos).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getWeightInKilos() {
        return weightInKilos;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    public ItemIdentifier getItemIdentifier() {
        return itemIdentifier;
    }
}
