package kata.supermarket;

import kata.supermarket.discounting.PricingDiscountScheme;

import java.math.BigDecimal;

public class Product {

    private final BigDecimal pricePerUnit;

    public Product(final BigDecimal pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    BigDecimal pricePerUnit() {
        return pricePerUnit;
    }

    public Item oneOf() {
        return new ItemByUnit(this);
    }

    public Item oneOf(PricingDiscountScheme pricingDiscountScheme) {
        return new ItemByUnit(this, pricingDiscountScheme);
    }
}
