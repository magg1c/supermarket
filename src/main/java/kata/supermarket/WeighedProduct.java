package kata.supermarket;

import java.math.BigDecimal;

public class WeighedProduct {

    private final BigDecimal pricePerKilo;

    public WeighedProduct(final BigDecimal pricePerKilo) {
        this.pricePerKilo = pricePerKilo;
    }

    BigDecimal pricePerKilo() {
        return pricePerKilo;
    }

    public Item weighing(final BigDecimal kilos) {
        return new ItemByWeight(this, kilos);
    }

    public Item weighing(final BigDecimal kilos, final Category category) {
        return new ItemByWeight(this, kilos, category);
    }

    public Item weighing(final BigDecimal kilos, final Category category, final ItemIdentifier itemIdentifier) {
        return new ItemByWeight(this, kilos, category, itemIdentifier);
    }
}
