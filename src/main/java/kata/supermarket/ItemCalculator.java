package kata.supermarket;

import java.math.BigDecimal;
import java.util.List;

public interface ItemCalculator {

    BigDecimal applyCalculations(List<Item> items);

}
