package kata.supermarket;

public enum ItemIdentifier {

    UNDEFINED,
    CARROT,
    CELERY;

}
