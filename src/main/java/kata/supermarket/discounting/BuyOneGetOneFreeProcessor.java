package kata.supermarket.discounting;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

public class BuyOneGetOneFreeProcessor extends PricingDiscountProcessor {

    public BuyOneGetOneFreeProcessor(final PricingDiscountProcessor nextProcessor) {
        super(nextProcessor);
    }

    @Override
    public ItemsDiscount getDiscount(final Item item, final List<Item> items) {

        if (item != null && items != null && item.getPricingDiscountScheme() == PricingDiscountScheme.BUY_ONE_GET_ONE_FREE) {

            List<Item> applicableItems = items.stream().filter(p -> p.getPricingDiscountScheme() == PricingDiscountScheme.BUY_ONE_GET_ONE_FREE).collect(Collectors.toList());

            if (applicableItems.size() > 1) {

                int oddItems = applicableItems.size() % 2;

                BigDecimal sumEvenItems = applicableItems.subList(0, applicableItems.size() - oddItems).stream().map(Item::price).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                BigDecimal discountSum = sumEvenItems.divide(new BigDecimal(2), RoundingMode.HALF_UP);

                return new ItemsDiscount(discountSum, applicableItems);
            }

        } else if (nextProcessor != null) {
            return nextProcessor.getDiscount(item, items);
        }

        return new ItemsDiscount(BigDecimal.ZERO, items);
    }
}
