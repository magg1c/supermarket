package kata.supermarket.discounting;

import kata.supermarket.Category;
import kata.supermarket.Item;
import kata.supermarket.ItemByWeight;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

public class BuyVegetablesForHalfPriceProcessor extends PricingDiscountProcessor {

    public BuyVegetablesForHalfPriceProcessor(PricingDiscountProcessor nextProcessor) {
        super(nextProcessor);
    }

    @Override
    public ItemsDiscount getDiscount(final Item item, List<Item> items) {

        if (items != null && item instanceof ItemByWeight && item.getCategory() == Category.VEGETABLE) {

            List<Item> applicableItems = items.stream().filter(p -> p instanceof ItemByWeight && p.getCategory() == Category.VEGETABLE).collect(Collectors.toList());

            List<ItemByWeight> applicableItemsByWeight = applicableItems.stream()
                                                                        .map(p -> (ItemByWeight)p)
                                                                        .collect(Collectors.toList());

            if (applicableItems.size() > 0) {

                BigDecimal discountedPrice = BigDecimal.ZERO;

                for (ItemByWeight itemByWeight : applicableItemsByWeight) {

                    if (itemByWeight.getWeightInKilos().compareTo(new BigDecimal("1")) >= 0) {

                        BigDecimal totalPricePerKilo = itemByWeight.getWeightInKilos().divide(new BigDecimal(2));
                        discountedPrice = discountedPrice.add(totalPricePerKilo);
                    }

                }

                return new ItemsDiscount(discountedPrice, applicableItems);

            } else {
                    return new ItemsDiscount(BigDecimal.ZERO, items);
            }

        } else if (nextProcessor != null) {
            return nextProcessor.getDiscount(item, items);
        }

        return new ItemsDiscount(BigDecimal.ZERO, items);
    }
}
