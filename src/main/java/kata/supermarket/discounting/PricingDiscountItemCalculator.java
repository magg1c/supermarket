package kata.supermarket.discounting;

import kata.supermarket.Item;
import kata.supermarket.ItemCalculator;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PricingDiscountItemCalculator implements ItemCalculator {

    private final PricingDiscountProcessor pricingDiscountProcessor;
    private final Set<Item> processedItems;
    private BigDecimal cumulativeDiscount;

    public PricingDiscountItemCalculator() {
        // TODO Move this constructor out to a Factory
        this.pricingDiscountProcessor = new BuyOneGetOneFreeProcessor(new BuyVegetablesForHalfPriceProcessor(null));
        this.processedItems = new HashSet<>();
        this.cumulativeDiscount = BigDecimal.ZERO;
    }

    @Override
    public BigDecimal applyCalculations(List<Item> items) {

        if (items == null)
            return BigDecimal.ZERO;

        // Iterate each item (skipping already processed items) and call PricingDiscountProcessor to perform the discount
        for (Item item : items) {
            if (!processedItems.contains(item)) {
                ItemsDiscount discount = pricingDiscountProcessor.getDiscount(item, items);
                processedItems.addAll(discount.getItems());

                cumulativeDiscount = cumulativeDiscount.add(discount.getDiscountedPrice());
            }
        }

        return cumulativeDiscount;
    }

}
