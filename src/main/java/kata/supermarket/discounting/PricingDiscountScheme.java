package kata.supermarket.discounting;

public enum PricingDiscountScheme {

    BUY_ONE_GET_ONE_FREE,
    BUY_TWO_ITEMS_FOR_ONE_POUND,
    BUY_THREE_ITEMS_FOR_PRICE_OF_TWO,
    NONE

}
