package kata.supermarket.discounting;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.util.List;

public class ItemsDiscount {

    private final BigDecimal discountedPrice;
    private final List<Item> items;

    public ItemsDiscount(BigDecimal discountedPrice, List<Item> items) {
        this.discountedPrice = discountedPrice;
        this.items = items;
    }

    public BigDecimal getDiscountedPrice() {
        return discountedPrice;
    }

    public List<Item> getItems() {
        return items;
    }
}
