package kata.supermarket.discounting;

import kata.supermarket.Item;

import java.util.List;

public abstract class PricingDiscountProcessor {

    public final PricingDiscountProcessor nextProcessor;

    public PricingDiscountProcessor(PricingDiscountProcessor nextProcessor) {
        this.nextProcessor = nextProcessor;
    }

    public abstract ItemsDiscount getDiscount(Item item, List<Item> items);


}
