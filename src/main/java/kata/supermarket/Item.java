package kata.supermarket;

import kata.supermarket.discounting.PricingDiscountScheme;

import java.math.BigDecimal;

public interface Item {
    BigDecimal price();
    default Category getCategory() {
        return Category.UNCLASSIFIED;
    }
    default PricingDiscountScheme getPricingDiscountScheme() {
        return PricingDiscountScheme.NONE;
    }
}
