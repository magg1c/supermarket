package kata.supermarket;

import kata.supermarket.discounting.PricingDiscountItemCalculator;
import kata.supermarket.discounting.PricingDiscountScheme;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BasketTest {

    @DisplayName("basket provides its total value when containing...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void basketProvidesTotalValue(String description, String expectedTotal, Iterable<Item> items) {
        final Basket basket = new Basket();
        items.forEach(basket::add);
        assertEquals(new BigDecimal(expectedTotal), basket.total());
    }

    static Stream<Arguments> basketProvidesTotalValue() {
        return Stream.of(
                noItems(),
                aSingleItemPricedPerUnit(),
                multipleItemsPricedPerUnit(),
                aSingleItemPricedByWeight(),
                multipleItemsPricedByWeight()
        );
    }

    @DisplayName("basket provides its total value with applied discounts when containing...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void basketProvidesTotalValueWithAppliedDiscounts(String description, String expectedTotal, Iterable<Item> items) {
        final Basket basket = new Basket();
        items.forEach(basket::add);
        assertEquals(new BigDecimal(expectedTotal), basket.total(new PricingDiscountItemCalculator()));
    }

    static Stream<Arguments> basketProvidesTotalValueWithAppliedDiscounts() {
        return Stream.of(
                aSingleItemPricedPerUnitWithDiscountBuyOneGetOneFree(),
                twoItemsPricedPerUnitWithDiscountBuyOneGetOneFree(),
                threeItemsPricedPerUnitWithDiscountBuyOneGetOneFree(),
                fourItemsPricedPerUnitWithDiscountBuyOneGetOneFree(),
                oneVegetableWeighingHalfAKilo(),
                twoVegetablesEachWeighingHalfAKilo(),
                threeVegetablesEachWeighingHalfAKilo(),
                fourVegetablesEachWeighingHalfAKilo(),
                fourVegetablesEachWeighingHalfAKiloAndAPintOfMilk(),
                fourVegetablesEachWeighingHalfAKiloAndTwoPintsOfMilk(),
                fourVegetablesEachWeighingHalfAKiloAndThreePintsOfMilk()
        );
    }


    private static Arguments aSingleItemPricedByWeight() {
        return Arguments.of("a single weighed item", "1.25", Collections.singleton(twoFiftyGramsOfAmericanSweets()));
    }

    private static Arguments multipleItemsPricedByWeight() {
        return Arguments.of("multiple weighed items", "1.85",
                Arrays.asList(twoFiftyGramsOfAmericanSweets(), twoHundredGramsOfPickAndMix())
        );
    }

    private static Arguments multipleItemsPricedPerUnit() {
        return Arguments.of("multiple items priced per unit", "2.04",
                Arrays.asList(aPackOfDigestives(), aPintOfMilk()));
    }

    private static Arguments aSingleItemPricedPerUnit() {
        return Arguments.of("a single item priced per unit", "0.49", Collections.singleton(aPintOfMilk()));
    }

    private static Arguments aSingleItemPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arguments.of("a single item priced per unit", "0.49", Collections.singleton(aPintOfMilkWithDiscountBuyOneGetOneFree()));
    }

    private static Arguments twoItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arguments.of("two items priced per unit", "0.49",
                Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree()));
    }

    private static Arguments threeItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arguments.of("three items priced per unit", "0.98",
                Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree()));
    }

    private static Arguments fourItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arguments.of("four items priced per unit", "0.98",
                Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree()));
    }

    private static Arguments oneVegetableWeighingHalfAKilo() {
        return Arguments.of("one vegetable weighing half a kilo", "1.00",
                Arrays.asList(halfAKiloOfVegetables()));
    }

    private static Arguments twoVegetablesEachWeighingHalfAKilo() {
        return Arguments.of("two vegetables each weighing half a kilo", "1.00",
                Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables()));
    }

    private static Arguments threeVegetablesEachWeighingHalfAKilo() {
        return Arguments.of("three vegetables each weighing half a kilo", "2.00",
                Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables()));
    }

    private static Arguments fourVegetablesEachWeighingHalfAKilo() {
        return Arguments.of("four vegetables each weighing half a kilo", "3.00",
                Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables()));
    }

    private static Arguments fourVegetablesEachWeighingHalfAKiloAndAPintOfMilk() {
        return Arguments.of("four vegetables each weighing half a kilo and a pint of milk", "3.49",
                Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables(), aPintOfMilkWithDiscountBuyOneGetOneFree()));
    }

    private static Arguments fourVegetablesEachWeighingHalfAKiloAndTwoPintsOfMilk() {
        return Arguments.of("four vegetables each weighing half a kilo and two pints of milk", "3.49",
                Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree()));
    }

    private static Arguments fourVegetablesEachWeighingHalfAKiloAndThreePintsOfMilk() {
        return Arguments.of("four vegetables each weighing half a kilo and three pints of milk", "3.98",
                Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree()));
    }

    private static Arguments noItems() {
        return Arguments.of("no items", "0.00", Collections.emptyList());
    }

    private static Item aPintOfMilk() {
        return new Product(new BigDecimal("0.49")).oneOf();
    }

    private static Item aPintOfMilkWithDiscountBuyOneGetOneFree() {
        return new Product(new BigDecimal("0.49")).oneOf(PricingDiscountScheme.BUY_ONE_GET_ONE_FREE);
    }

    private static Item aPackOfDigestives() {
        return new Product(new BigDecimal("1.55")).oneOf();
    }

    private static WeighedProduct aKiloOfAmericanSweets() {
        return new WeighedProduct(new BigDecimal("4.99"));
    }

    private static Item twoFiftyGramsOfAmericanSweets() {
        return aKiloOfAmericanSweets().weighing(new BigDecimal(".25"));
    }

    private static WeighedProduct aKiloOfPickAndMix() {
        return new WeighedProduct(new BigDecimal("2.99"));
    }

    private static Item halfAKiloOfVegetables() {
        return new WeighedProduct(new BigDecimal("2.00")).weighing(new BigDecimal(".5"), Category.VEGETABLE);
    }

    private static Item twoHundredGramsOfPickAndMix() {
        return aKiloOfPickAndMix().weighing(new BigDecimal(".2"));
    }
}