package kata.supermarket;

import kata.supermarket.discounting.PricingDiscountItemCalculator;
import kata.supermarket.discounting.PricingDiscountScheme;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PricingDiscountItemCalculatorTest {

    @ParameterizedTest(name = "{0}")
    @MethodSource
    void pricingDiscountItemCalculatorHasExpectedItemsDiscount(String description, List<Item> itemsList, BigDecimal expectedDiscountPrice) {
        final PricingDiscountItemCalculator pricingDiscountItemCalculator = new PricingDiscountItemCalculator();
        BigDecimal discount = pricingDiscountItemCalculator.applyCalculations(itemsList);
        assertEquals(expectedDiscountPrice, discount);
    }

    static Stream<Arguments> pricingDiscountItemCalculatorHasExpectedItemsDiscount() {
        return Stream.of(
                Arguments.of("None", null, "0"),
                Arguments.of("Item List Null", null, "0"),
                Arguments.of("Item List Empty", new ArrayList<>(), "0"),
                Arguments.of("Five Items", fiveItemsPricedPerUnitWithDiscountBuyOneGetOneFree(), "0.98"),
                Arguments.of("Four Items", fourItemsPricedPerUnitWithDiscountBuyOneGetOneFree(), "0.98"),
                Arguments.of("Three Items", threeItemsPricedPerUnitWithDiscountBuyOneGetOneFree(), "0.49"),
                Arguments.of("Two Items", twoItemsPricedPerUnitWithDiscountBuyOneGetOneFree(), "0.49")
        );
    }

    private static Item aPintOfMilkWithDiscountBuyOneGetOneFree() {
        return new Product(new BigDecimal("0.49")).oneOf(PricingDiscountScheme.BUY_ONE_GET_ONE_FREE);
    }

    private static List<Item> twoItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree());
    }

    private static List<Item> threeItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree());
    }

    private static List<Item> fourItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree());
    }

    private static List<Item> fiveItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree());
    }

}
