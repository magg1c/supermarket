package kata.supermarket;

import kata.supermarket.discounting.BuyVegetablesForHalfPriceProcessor;
import kata.supermarket.discounting.ItemsDiscount;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BuyVegetablesForHalfPriceProcessorTest {

    @ParameterizedTest(name = "{0}")
    @MethodSource
    void buyOneKiloOfVegetablesForHalfPriceProcessorHasExpectedItemsDiscount(String description, Item item, List<Item> itemsList, BigDecimal expectedDiscountPrice) {
        final BuyVegetablesForHalfPriceProcessor buyVegetablesForHalfPriceProcessor = new BuyVegetablesForHalfPriceProcessor(null);
        ItemsDiscount itemsDiscount = buyVegetablesForHalfPriceProcessor.getDiscount(item, itemsList);
        assertEquals(expectedDiscountPrice, itemsDiscount.getDiscountedPrice());
    }

    static Stream<Arguments> buyOneKiloOfVegetablesForHalfPriceProcessorHasExpectedItemsDiscount() {
        return Stream.of(
                //Arguments.of("None", null, null, "0"),
                //Arguments.of("Item List Null", halfAKiloOfVegetables(), null, "0"),
                //Arguments.of("Item List Empty", halfAKiloOfVegetables(), new ArrayList<>(), "0"),
                //Arguments.of("Four Items", halfAKiloOfVegetables(), fourVegetablesEachWeighingHalfAKilo(), "1.00"),
                //Arguments.of("Three Items", halfAKiloOfVegetables(), threeVegetablesEachWeighingHalfAKilo(), "1.00"),
                Arguments.of("Two and a Half Kilos of Celery", twoAndAHalfKilosOfCellery(), twoLotsOfTwoAndAHalfKilosOfCellery(), "2.50"),
                Arguments.of("Two Celery, Two Kilo Of Carrots", twoKiloOfCellery(), twoLotsOfAKiloOfCelleryAndTwoLotsOfAKiloCarrots(), "3.0"),
                Arguments.of("Two Celery, Two Half Kilo Of Carrots", twoKiloOfCellery(), twoLotsOfAKiloOfCelleryAndTwoLotsOfHalfKiloCarrots(), "2"),
                Arguments.of("Four Items", twoKiloOfCellery(), fourLotsOfCellery(), "4"),
                Arguments.of("Three Items", twoKiloOfCellery(), threeLotsOfCellery(), "3"),
                Arguments.of("Two Items", twoKiloOfCellery(), twoLotsOfCellery(), "2")
        );
    }

    private static Item aKiloOfCarrots() {
        return new WeighedProduct(new BigDecimal("2.00")).weighing(new BigDecimal("1"), Category.VEGETABLE, ItemIdentifier.CARROT);
    }

    private static Item halfAKiloOfCarrots() {
        return new WeighedProduct(new BigDecimal("2.00")).weighing(new BigDecimal(".5"), Category.VEGETABLE, ItemIdentifier.CARROT);
    }

    private static Item twoAndAHalfKilosOfCellery() {
        return new WeighedProduct(new BigDecimal("1.00")).weighing(new BigDecimal("2.5"), Category.VEGETABLE, ItemIdentifier.CELERY);
    }

    private static Item twoKiloOfCellery() {
        return new WeighedProduct(new BigDecimal("1.00")).weighing(new BigDecimal("2"), Category.VEGETABLE, ItemIdentifier.CELERY);
    }

    private static Item halfAKiloOfVegetables() {
        return new WeighedProduct(new BigDecimal("2.00")).weighing(new BigDecimal(".5"), Category.VEGETABLE);
    }

    private static List<Item> fourVegetablesEachWeighingHalfAKilo() {
        return Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables());
    }

    private static List<Item> threeVegetablesEachWeighingHalfAKilo() {
        return Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables(), halfAKiloOfVegetables());
    }

    private static List<Item> twoVegetablesEachWeighingHalfAKilo() {
        return Arrays.asList(halfAKiloOfVegetables(), halfAKiloOfVegetables());
    }

    private static List<Item> fourLotsOfCellery() {
        return Arrays.asList(twoKiloOfCellery(), twoKiloOfCellery(), twoKiloOfCellery(), twoKiloOfCellery());
    }

    private static List<Item> threeLotsOfCellery() {
        return Arrays.asList(twoKiloOfCellery(), twoKiloOfCellery(), twoKiloOfCellery());
    }

    private static List<Item> twoLotsOfCellery() {
        return Arrays.asList(twoKiloOfCellery(), twoKiloOfCellery());
    }

    private static List<Item> twoLotsOfAKiloOfCelleryAndTwoLotsOfHalfKiloCarrots() {
        return Arrays.asList(twoKiloOfCellery(), twoKiloOfCellery(), halfAKiloOfCarrots(), halfAKiloOfCarrots());
    }

    private static List<Item> twoLotsOfAKiloOfCelleryAndTwoLotsOfAKiloCarrots() {
        return Arrays.asList(twoKiloOfCellery(), twoKiloOfCellery(), aKiloOfCarrots(), aKiloOfCarrots());
    }

    private static List<Item> twoLotsOfTwoAndAHalfKilosOfCellery() {
        return Arrays.asList(twoAndAHalfKilosOfCellery(), twoAndAHalfKilosOfCellery());
    }

}
