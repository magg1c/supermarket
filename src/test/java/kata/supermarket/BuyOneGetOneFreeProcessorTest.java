package kata.supermarket;

import kata.supermarket.discounting.BuyOneGetOneFreeProcessor;
import kata.supermarket.discounting.ItemsDiscount;
import kata.supermarket.discounting.PricingDiscountScheme;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BuyOneGetOneFreeProcessorTest {

    @ParameterizedTest(name = "{0}")
    @MethodSource
    void buyOneGetOneFreeProcessorHasExpectedItemsDiscount(String description, Item item, List<Item> itemsList, BigDecimal expectedDiscountPrice) {
        final BuyOneGetOneFreeProcessor buyOneGetOneFreeProcessor = new BuyOneGetOneFreeProcessor(null);
        ItemsDiscount itemsDiscount = buyOneGetOneFreeProcessor.getDiscount(item, itemsList);
        assertEquals(expectedDiscountPrice, itemsDiscount.getDiscountedPrice());
    }

    static Stream<Arguments> buyOneGetOneFreeProcessorHasExpectedItemsDiscount() {
        return Stream.of(
                Arguments.of("None", null, null, "0"),
                Arguments.of("Item List Null", aPintOfMilkWithDiscountBuyOneGetOneFree(), null, "0"),
                Arguments.of("Item List Empty", aPintOfMilkWithDiscountBuyOneGetOneFree(), new ArrayList<>(), "0"),
                Arguments.of("Five Items", aPintOfMilkWithDiscountBuyOneGetOneFree(), fiveItemsPricedPerUnitWithDiscountBuyOneGetOneFree(), "0.98"),
                Arguments.of("Four Items", aPintOfMilkWithDiscountBuyOneGetOneFree(), fourItemsPricedPerUnitWithDiscountBuyOneGetOneFree(), "0.98"),
                Arguments.of("Three Items", aPintOfMilkWithDiscountBuyOneGetOneFree(), threeItemsPricedPerUnitWithDiscountBuyOneGetOneFree(), "0.49"),
                Arguments.of("Two Items", aPintOfMilkWithDiscountBuyOneGetOneFree(), twoItemsPricedPerUnitWithDiscountBuyOneGetOneFree(), "0.49")
        );
    }

    private static Item aPintOfMilkWithDiscountBuyOneGetOneFree() {
        return new Product(new BigDecimal("0.49")).oneOf(PricingDiscountScheme.BUY_ONE_GET_ONE_FREE);
    }

    private static List<Item> twoItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree());
    }

    private static List<Item> threeItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree());
    }

    private static List<Item> fourItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree());
    }

    private static List<Item> fiveItemsPricedPerUnitWithDiscountBuyOneGetOneFree() {
        return Arrays.asList(aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree(), aPintOfMilkWithDiscountBuyOneGetOneFree());
    }
}
